class Intcode:
    def __init__(self, intcode, optcodes):
        self.address = 0
        self.intcode = intcode
        self.optcodes = optcodes
    @classmethod
    def run(self):
        while self.address != "stop":
            if self.intcode[self.address] in self.optcodes.keys():
                instruction = self.optcodes[self.intcode[self.address]]
                updated = instruction(intcode = self.intcode, pointer = self.address).run()
                self.intcode = updated["intcode"]
                self.address = updated["address"]
            else:
                break
        return self.intcode