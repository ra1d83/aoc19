## Advent of Code 2019
## Day 1

def get_fuel(m):
    fuel = (m // 3) - 2
    return fuel

def get_fuel_recursive(m, tot = 0):
    if get_fuel(m) <= 0 :
        return tot
    else:
        return get_fuel_recursive(get_fuel(m), tot + get_fuel(m))

def test_get_fuel():
    assert get_fuel(14) == 2
    assert get_fuel(12) == 2
    assert get_fuel(1969) == 654
    assert get_fuel(756) == 33583
    print("All good")

test_get_fuel()

def test_get_fuel_recursive():
    assert get_fuel_recursive(14) == 2
    assert get_fuel_recursive(1969) == 966
    assert get_fuel_recursive(100756) == 50346
    print("All good")
test_get_fuel_recursive()

def sum_module_fuel(l):
    mod_fuel = 0
    for mod in l:
        mod_fuel += get_fuel(mod)
    return mod_fuel

def sum_module_fuel_rec(l):
    mod_fuel = 0
    for mod in l:
        mod_fuel += get_fuel_recursive(mod)
    return mod_fuel
# https://adventofcode.com/2019/day/1/input
mods = [83133,
        130874,
        140147,
        117477,
        144367,
        54627,
        133133,
        65928,
        76778,
        102928,
        135987,
        125674,
        74597,
        136246,
        117771,
        92413,
        64422,
        56693,
        92601,
        54694,
        95137,
        86188,
        126454,
        99142,
        94487,
        53785,
        69679,
        123479,
        124598,
        121152,
        146564,
        101173,
        82025,
        55187,
        84083,
        69403,
        114456,
        84722,
        88667,
        80619,
        121281,
        118139,
        125808,
        54034,
        81780,
        116401,
        136396,
        137830,
        108481,
        103712,
        144950,
        85621,
        57973,
        99549,
        107704,
        115782,
        83445,
        91681,
        87607,
        52745,
        76839,
        61881,
        73658,
        102315,
        100651,
        72929,
        124015,
        134764,
        135088,
        127294,
        66563,
        100125,
        83062,
        91212,
        143130,
        78993,
        58940,
        120981,
        110504,
        142779,
        95328,
        135936,
        84490,
        112005,
        101554,
        111185,
        124249,
        126525,
        96909,
        145482,
        140368,
        83014,
        77784,
        130376,
        79031,
        122317,
        100188,
        66679,
        89074,
        120969
        ]

print("Part 1 result: %s" %(sum_module_fuel(mods)))
print("Part 2 result: %s " %(sum_module_fuel_rec(mods)))