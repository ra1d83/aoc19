## Advent of Code 2019 Day 22
## author: Simone Daminelli

from AoC19_22_funs import *
test_input = list(range(0,10,1))


## Part 1 solution
part1_deck = list(range(0,10007, 1))
part1_puzzle = 'AoC19_Python/AoC19_22/puzzle_input_day22.txt'

test_shuffle_deck()

part1_answer = shuffle_deck(deck = part1_deck, input_file = part1_puzzle)
print("Part 1 answer: %d" % part1_answer.index(2019))
# Part 1 answer: 1252

## Part 2
## Solution from Reddit
# https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/
## Relies on https://en.wikipedia.org/wiki/Modular_arithmetic
m = 119315717514047
n = 101741582076661
pos = 2020
shuffles = { 'deal with increment ': lambda x,m,a,b: (a*x %m, b*x %m),
         'deal into new stack': lambda _,m,a,b: (-a %m, (m-1-b)%m),
         'cut ': lambda x,m,a,b: (a, (b-x)%m) }
a,b = 1,0
with open(part1_puzzle) as f:
  for s in f.read().strip().split('\n'):
    for name,fn in shuffles.items():
      if s.startswith(name):
        arg = int(s[len(name):]) if name[-1] == ' ' else 0
        a,b = fn(arg, m, a, b)
        break
# https://www.programiz.com/python-programming/methods/built-in/pow
r = (b * pow(1-a, m-2, m)) % m
print(f"Card at #{pos}: {((pos - r) * pow(a, n*(m-2), m) + r) % m}")