## Advent of Code 2019
## Day 2

puzzle_input = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,2,19,13,23,
                1,23,10,27,1,13,27,31,2,31,10,35,1,35,9,39,1,39,13,43,
                1,13,43,47,1,47,13,51,1,13,51,55,1,5,55,59,2,10,59,63,
                1,9,63,67,1,6,67,71,2,71,13,75,2,75,13,79,1,79,9,83,2,
                83,10,87,1,9,87,91,1,6,91,95,1,95,10,99,1,99,13,103,1,
                13,103,107,2,13,107,111,1,111,9,115,2,115,10,119,1,119,
                5,123,1,123,2,127,1,127,5,0,99,2,14,0,0]
def copy_input(l, fix):
    new_list = l[:]
    if fix:
        new_list[1]=12
        new_list[2]=2
    return new_list
fixed_puzzle_input = copy_input(puzzle_input, fix = True)

goal = 19690720
r = range(0,100, 1)

##======================================================================================================================
## Refactored code - Part 1 and Part 2
##======================================================================================================================
class Instruction:
    """Default class for instructions"""
    def __init__(self, intcode, pointer, parameters = 1):
        self.intcode = intcode
        self.pointer = pointer
        self.parameters = parameters
    def countparameters(self):
        return len(self.parameters)
    def nextpointer(self):
        return self.pointer + self.countparameters() + 1
    def updateintcode(self):
        return self.intcode
    def run(self):
        newintcode = self.updateintcode()
        address = self.nextpointer()
        output = {"address": address, "intcode": newintcode}
        return output

class Optcode_add(Instruction):
    def __init__(self, intcode, pointer):
        try:
            parameters = intcode[(pointer+1):(pointer+4)]
        except:
            intcode = intcode.extend([99,0,0,0])
            parameters = intcode[(pointer+1):(pointer+4)]
        super().__init__(intcode, pointer, parameters = parameters)
    def updateintcode(self):
        newintcode = self.intcode
        out_ind = self.parameters[2]
        try:
            out_val = self.intcode.__getitem__(self.parameters[0]) + self.intcode.__getitem__(self.parameters[1])
            newintcode[out_ind] = out_val
            return newintcode
        except IndexError:
            return self.intcode.extend([99,0,0,0])

class Optcode_mult(Instruction):
    def __init__(self, intcode, pointer):
        try:
            parameters = intcode[(pointer + 1):(pointer + 4)]
        except:
            intcode = intcode.extend([99, 0, 0, 0])
            parameters = intcode[(pointer + 1):(pointer + 4)]
        super().__init__(intcode, pointer, parameters=parameters)
    def updateintcode(self):
        newintcode = self.intcode
        out_ind = self.parameters[2]
        try:
            out_val = self.intcode.__getitem__(self.parameters[0]) * self.intcode.__getitem__(self.parameters[1])
            newintcode[out_ind] = out_val
            return newintcode
        except IndexError:
            return self.intcode.extend([99,0,0,0])

class Optcode_stop(Instruction):
    def __init__(self, intcode, pointer):
        super().__init__(intcode, pointer, parameters = 1)
    def run(self):
        output = {"address": "stop", "intcode": self.updateintcode()}
        return output

def test_Instruction():
    assert Optcode_mult([2,3,0,3,99], pointer = 0).run() == {'address': 4, 'intcode': [2, 3, 0, 6, 99]}
    assert Optcode_add([1,0,0,0,99], pointer = 0).run() ==  {'address': 4, 'intcode': [2, 0, 0, 0, 99]}
    assert Optcode_mult([1, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50], pointer=4).run() == {'address': 8, 'intcode': [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]}
    print("All good")
test_Instruction()

optcodes = {1: Optcode_add, 2: Optcode_mult, 99: Optcode_stop}

class Intcode:
    def __init__(self, intcode, optcodes):
        self.address = 0
        self.intcode = intcode
        self.optcodes = optcodes
    def run(self):
        while self.address != "stop":
            if self.intcode[self.address] in optcodes.keys():
                instruction = self.optcodes[self.intcode[self.address]]
                updated = instruction(intcode = self.intcode, pointer = self.address).run()
                self.intcode = updated["intcode"]
                self.address = updated["address"]
            else:
                break
        return self.intcode
def test_Intcode():
    test_intcode = Intcode([1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50], optcodes = optcodes)
    assert test_intcode.run() == [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]
    print("All good")
test_Intcode()

first_intcode = Intcode(fixed_puzzle_input, optcodes = optcodes)

answer_part1 = first_intcode.run()[0]
print("Answet to part1 %s" %answer_part1)

def find_goal(goal, r, input):
    for noun in r:
        for verb in r:
            input[1] = noun
            input[2] = verb
            tmp_intcode = Intcode(copy_input(input, fix = False), optcodes = optcodes)
            try:
                out = tmp_intcode.run()
                if out[0] == goal:
                    print("Answer to part2 %d" % (100 * noun + verb))
            except:
                pass

find_goal(goal = 19690720, r = range(0,100,1), input = puzzle_input)
