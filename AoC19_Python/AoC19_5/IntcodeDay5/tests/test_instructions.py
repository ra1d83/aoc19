import unittest
from IntcodeDay5.instructions import *

class instructionsTestCase(unittest.TestCase):
    """Tests for `instructions.py`."""
    def test_Instruction():
        assert Optcode_mult([2,3,0,3,99], pointer = 0).run() == {'address': 4, 'intcode': [2, 3, 0, 6, 99]}
        assert Optcode_add([1,0,0,0,99], pointer = 0).run() ==  {'address': 4, 'intcode': [2, 0, 0, 0, 99]}
        assert Optcode_mult([1, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50], pointer=4).run() == {'address': 8, 'intcode': [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]}