import unittest
from IntcodeDay5.intcode import Intcode
from IntcodeDay5.instructions import *

class IntcodeTestCase(unittest.TestCase):
    """Tests for `intcode.py`."""
    def test_Intcode(self, intcode = [1, 9, 10, 3, 2, 3, 11, 0, 99, 30, 40, 50], optcodes = {1: Optcode_add, 2: Optcode_mult, 99: Optcode_stop}):
        test_intcode = Intcode(intcode = intcode, optcodes = optcodes)
        assert test_intcode.run() == [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]
