def opt_add(a,b):
    return a+b

def opt_mult(a,b):
    return a*b

def opt_break(a,b):
    pass

optcodes = {1: opt_add, 2: opt_mult, "stop": 99}

def apply_optcodes(l):
    for i in range(0,len(l)-3,4):
        if l[i] != optcodes["stop"]:
            l[l[i+3]] = optcodes[l[i]](l[l[i+1]], l[l[i+2]])
    return l

def test_apply_optcodes():
    assert apply_optcodes([1,9,10,3,2,3,11,0,99,30,40,50]) == [3500, 9, 10, 70, 2, 3, 11, 0, 99, 30, 40, 50]
    assert apply_optcodes([1,0,0,0,99]) == [2, 0, 0, 0, 99]
    assert apply_optcodes([2,4,4,5,99,0]) == [2, 4, 4, 5, 99, 9801]
    assert apply_optcodes([1,1,1,4,99,5,6,0,99]) == [30, 1, 1, 4, 2, 5, 6, 0, 99]
    print("All good")

puzzle_input = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,2,19,13,23,
                1,23,10,27,1,13,27,31,2,31,10,35,1,35,9,39,1,39,13,43,
                1,13,43,47,1,47,13,51,1,13,51,55,1,5,55,59,2,10,59,63,
                1,9,63,67,1,6,67,71,2,71,13,75,2,75,13,79,1,79,9,83,2,
                83,10,87,1,9,87,91,1,6,91,95,1,95,10,99,1,99,13,103,1,
                13,103,107,2,13,107,111,1,111,9,115,2,115,10,119,1,119,
                5,123,1,123,2,127,1,127,5,0,99,2,14,0,0]
def copy_input(l, fix):
    new_list = l[:]
    if fix:
        new_list[1]=12
        new_list[2]=2
    return new_list
fixed_puzzle_input = copy_input(puzzle_input, fix = True)
#puzzle_input[1] = 12
#puzzle_input[2] = 2
part1_answer = apply_optcodes(fixed_puzzle_input)[0]
print("Answet to part1 %s" %part1_answer)
