import setuptools

with open("IntcodeDay5/README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="IntcodeDay5",
    version="0.0.1",
    author="Simone Daminelli",
    author_email="ra1d83@gmail.com",
    description="Incode package for Advent of Code 2019 Day 5",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/ra1d83/aoc19.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)