class Instruction:
    """Default class for instructions"""
    def __init__(self, intcode, pointer, parameters = 1):
        self.intcode = intcode
        self.pointer = pointer
        self.parameters = parameters
    def countparameters(self):
        return len(self.parameters)
    def nextpointer(self):
        return self.pointer + self.countparameters() + 1
    def updateintcode(self):
        return self.intcode
    def run(self):
        newintcode = self.updateintcode()
        address = self.nextpointer()
        output = {"address": address, "intcode": newintcode}
        return output

class Optcode_add(Instruction):
    def __init__(self, intcode, pointer):
        try:
            parameters = intcode[(pointer+1):(pointer+4)]
        except:
            intcode = intcode.extend([99,0,0,0])
            parameters = intcode[(pointer+1):(pointer+4)]
        super().__init__(intcode, pointer, parameters = parameters)
    def updateintcode(self):
        newintcode = self.intcode
        out_ind = self.parameters[2]
        try:
            out_val = self.intcode.__getitem__(self.parameters[0]) + self.intcode.__getitem__(self.parameters[1])
            newintcode[out_ind] = out_val
            return newintcode
        except IndexError:
            return self.intcode.extend([99,0,0,0])

class Optcode_mult(Instruction):
    def __init__(self, intcode, pointer):
        try:
            parameters = intcode[(pointer + 1):(pointer + 4)]
        except:
            intcode = intcode.extend([99, 0, 0, 0])
            parameters = intcode[(pointer + 1):(pointer + 4)]
        super().__init__(intcode, pointer, parameters=parameters)
    def updateintcode(self):
        newintcode = self.intcode
        out_ind = self.parameters[2]
        try:
            out_val = self.intcode.__getitem__(self.parameters[0]) * self.intcode.__getitem__(self.parameters[1])
            newintcode[out_ind] = out_val
            return newintcode
        except IndexError:
            return self.intcode.extend([99,0,0,0])

class Optcode_stop(Instruction):
    def __init__(self, intcode, pointer):
        super().__init__(intcode, pointer, parameters = 1)
    def run(self):
        output = {"address": "stop", "intcode": self.updateintcode()}
        return output