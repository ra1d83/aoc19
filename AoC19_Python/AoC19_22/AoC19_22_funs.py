## Advent of Code 2019 Day 22
## author: Simone Daminelli

import re

def deal_into_new_stack(l) :
    return l[::-1]

def cut(l, n):
    if n >0:
        cut = l[:n]
        stack = l[n:]
        out = stack + cut
    else:
        cut = l[n:]
        stack = l[:n]
        out = cut + stack
    return out

def deal_with_increment(l, n):
    order = []
    max_r = 0
    while len(order) < len(l):
        myorder = list(range(max_r, len(l), n))
        order += myorder
        max_r = max(myorder)+n-len(l)
    l = sorted(zip(l, order), key=lambda x: x[1])
    return [item[0] for item in l]

def shuffle_deck(deck, input_file):
    input_file = open(input_file).readlines()
    for l in input_file:
        l = l.strip()
        param = re.search(r'-?\d+$', l)
        if param == None:
            fun = globals()[l.replace(" ", "_")]
            deck = fun(deck)
        else:
            l = l.replace(param.group(), "").strip()
            fun = globals()[l.replace(" ", "_")]
            deck = fun(deck, int(param.group()))
    return deck

def test_shuffle_deck():
    assert shuffle_deck(deck = list(range(0,10,1)), input_file = 'AoC19_Python/AoC19_22/test1.txt') == [0,3,6,9,2,5,8,1,4,7]
    print("Test 1 ok\n")
    assert shuffle_deck(deck = list(range(0,10,1)), input_file = 'AoC19_Python/AoC19_22/test2.txt') == [3,0,7,4,1,8,5,2,9,6]
    print("Test 2 ok\n")
    assert shuffle_deck(deck = list(range(0,10,1)), input_file = 'AoC19_Python/AoC19_22/test4.txt') == [9,2,5,8,1,4,7,0,3,6]
    print("Test 3 ok\n")
